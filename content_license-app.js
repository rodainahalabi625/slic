"use strict";

chrome.runtime.onMessage.addListener(handleMessage);

function handleMessage (message, sender, sendResponse) {
  if (message.request === "scrape") {
    let info = collectInfo(message.id);
    sendResponse({ data: info });  
  } else if (message.request === "clipboard") {
    navigator.clipboard.writeText(message.clipboard).then( () => {
        notifySuccess("Copy successful");
      }, () => {
        notifyError("Copy failed");
      });
  }
  return true;
};

function notifyError(message) {
  alertify.error(message, 3, () => { console.log(message); });
}

function notifySuccess(message) {
  alertify.success(message, 3);
}

function collectInfo(tabid) {
  let info = {};
  info.tabid = tabid;
  let url = document.URL;
  if (url.startsWith("https://license.gitlab.com/licenses/")) {
    collectInfoLicenseApp(info);
  } else {
    info.page = "unknown"
  }
  return info;
}

function collectInfoLicenseApp(info) {
  info.page = "LicenseApp"
  info.url = document.URL;
  // console.log("Collect LicenseApp info");
  // No id's, so ugly fragile parsing needed
  // Two columns contain dl's
  let dt = document.querySelectorAll("div.container-fluid.mt-3 div.row.mt-3 div.col-md-6 dl dt");
  let dd = document.querySelectorAll("div.container-fluid.mt-3 div.row.mt-3 div.col-md-6 dl dd");
  if (dt.length == dd.length) {
    for (let i = 0; i < dt.length; i++) {
      // Field name, eg "First name" -> remove LF/CR, trailing ':', lowercase, snake_case
      let key = dt[i].textContent.trim().toLowerCase().replace(/ /g, "_").replace(/:$/, "");
      // Field value, eg "Jabberwocky" -> remove LF/CR
      let value = dd[i].textContent.trim();
      //console.log(`${key} = ${value}`);
      info[key] = value;
    }
  } else {
    notifyError("Error: dt and dd length not equal");
  }

}