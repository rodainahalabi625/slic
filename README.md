# slic

Subscription and License Information Copier

## Deprecated

The slic browser extension will be replaced by a user script in the [punk](https://gitlab.com/rverschoor/punk) project.

## Purpose

While working on the License & Renewal queue (L&R) I regularly found myself manually copying information from the [Customers Portal](https://customers.gitlab.com/admin) and [LicenseApp](https://license.gitlab.com/licenses/) into internal notes in the support ticket.
This browser extension will do the copying for you, and let it paste you nicely formatted.

## Installation

Firefox: Get the extension from https://addons.mozilla.org/addon/slic/ \
Chrome, Opera, Vivaldi, etc.: Get the extension from https://chrome.google.com/webstore/detail/slic/hbbobedddedlnpokigieedmphgpahghn

## Usage

The extension works on the following pages:

- LicenseApp while showing a license
- Customers Portal, in the Show, Edit, and Impersonate pages.

When you're on one of these pages click the `slic` icon in the toolbar.
Information from the page is then copied to the clipboard, and ready to be pasted in e.g. a Zendesk ticket.

## Templates

Slic uses the [mustache.js](https://github.com/janl/mustache.js) template system to format the information.

You can change the templates to your personal taste.
- Click the menu button in Firefox and select `Add-ons`.
- Select `Extensions` in the left-side panel.
- Click the gear icon in the upper-right area of the Add-on Manager Extensions panel. 
- Find slic in the list, click on `...`, and select `Preferences`.

Use the `Save` button to save any changes you make to a template. The `Default` button will revert the template back to the factory setting.

## Limitations

Some limitations:

- It doesn't scrape all information yet.

See the [issue tracker](https://gitlab.com/rverschoor/slic/-/issues).


Icon credit: https://uxwing.com/stack-icon/ \
Libraries used:
- AlertifyJS 1.13.1
- Mustache 3.1.0
- WebExtension browser API Polyfill 0.7.0
