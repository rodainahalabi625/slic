"use strict";

chrome.runtime.onMessage.addListener(handleMessage);

function handleMessage (message, sender, sendResponse) {
  if (message.request === "scrape") {
    let info = collectInfo(message.id);
    sendResponse({ data: info });  
  } else if (message.request === "clipboard") {
    navigator.clipboard.writeText(message.clipboard).then( () => {
        notifySuccess("Copy successful");
      }, () => {
        notifyError("Copy failed");
      });
  }
  return true;
};

function notifyError(message) {
  alertify.error(message, 3, () => { console.log(message); });
}

function notifySuccess(message) {
  alertify.success(message, 3);
}

function collectInfo(tabid) {
  let info = {};
  info.tabid = tabid;
  collectInfoTest(info);
  return info;
}

function collectInfoTest(info) {
  info.page = "Test"
  info.url = document.URL;
  info.projects = [];
  let projects = document.querySelectorAll("div.project-details span.project-name");
  for (let p of projects) {
    info.projects.push({title: p.textContent});
  }
}
