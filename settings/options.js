"use strict";

function showStatus(name, text) {
  var status = document.getElementById("status-" + name);
  status.innerHTML = text; setTimeout( () => { status.innerHTML = ""; }, 4000);
}

function saveTemplate(name, action) {
  let text = document.querySelector("#template-" + name).value;
  chrome.storage.local.set({ ["template-" + name]: text }, (result) => {
    if (chrome.runtime.lastError) {
      console.warn("Slic error: " + chrome.runtime.lastError.message);
      showStatus(name, 'Error loading template: ' + chrome.runtime.lastError.message);
    } else {
      showStatus(name, 'Template ' + action);
    }
  });
}

function restoreDefaultTemplate(name) {
  document.querySelector("#template-" + name).value = DEFAULT_TEMPLATE["template-" + name];
  saveTemplate(name, "restored");
}

function restoreOptions() {
  PAGES.forEach( (pagename) => {
    let templatename = "template-" + pagename;
    chrome.storage.local.get([templatename], (result) => {
      if (chrome.runtime.lastError) {
        console.warn("Slic error: " + chrome.runtime.lastError.message);
        showStatus(pagename, 'Error restoring template: ' + chrome.runtime.lastError.message);
      } else {
        let value = result[templatename];
        if (value === undefined) {
          value = DEFAULT_TEMPLATE[templatename];
          showStatus(pagename, 'Template set to default');
        } 
        document.querySelector("#" + templatename).value = value;
        showStatus(pagename, 'Template restored');
      }
    });
  });
}

function listenSaveButton(name) {
  const button = document.querySelector("#save-" + name);
  button.addEventListener("click", () => {
    saveTemplate(name, "saved"); 
  });
}

function listenDefaultButton(name) {
  const button = document.querySelector("#default-" + name);
  button.addEventListener("click", () => {
    restoreDefaultTemplate(name); 
  });
}

function listenButtons() {
  PAGES.forEach( (name) => {
    listenSaveButton(name); 
    listenDefaultButton(name); 
  });
}

function initialize() {
  document.addEventListener("DOMContentLoaded", restoreOptions);
  document.addEventListener("DOMContentLoaded", listenButtons);
}

initialize();
